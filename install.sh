#!/bin/bash

apt-get update
yes | apt-get install golang-1.10
go get github.com/BurntSushi/toml
go get github.com/hashicorp/raft
