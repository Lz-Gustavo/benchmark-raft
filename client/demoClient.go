package main

import (
	"fmt"
	"net"

	"github.com/BurntSushi/toml"
)

// Info struct stores the server configuration initialized by every
// client.
type Info struct {
	Rep    int
	SvrIps []string

	Svrs []net.Conn
}

// New instatiates a new client dummy, that just sends messages to the
// replicated service. Info is configured by specified toml file.
func New(config string) (*Info, error) {

	client := &Info{}
	_, err := toml.DecodeFile(config, client)
	if err != nil {
		return nil, err
	}
	return client, nil
}

// Connect creates a tcp connection to every replica on the fsm
func (client *Info) Connect() error {

	client.Svrs = make([]net.Conn, client.Rep)
	var err error

	for i, v := range client.SvrIps {
		client.Svrs[i], err = net.Dial("tcp", v)
		if err != nil {
			return err
		}
	}
	return nil
}

// Disconnect closes every open socket connection with the fsm cluster
func (client *Info) Disconnect() {
	for _, v := range client.Svrs {
		v.Close()
	}
}

// Broadcast a message to the cluster
func (client *Info) Broadcast(message string) error {
	for _, v := range client.Svrs {
		_, err := fmt.Fprint(v, message)
		if err != nil {
			return err
		}
	}
	return nil
}

func main() {}
