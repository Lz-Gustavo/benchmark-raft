package main

import (
	"runtime"
	"sync"
	"testing"
	"time"
)

const (
	numClients      = 1
	execTimeSeconds = 120
)

func TestOrderReq(t *testing.T) {

	configBarrier := new(sync.WaitGroup)
	configBarrier.Add(numClients)

	finishedBarrier := new(sync.WaitGroup)
	finishedBarrier.Add(numClients)

	clients := make([]*Info, numClients, numClients)
	signal := make(chan bool)
	requests := make(chan string)

	go generateStrRequests(requests, signal)
	go killWorkers(execTimeSeconds, signal)

	for i := 0; i < numClients; i++ {
		go func(j int, requests chan string, kill chan bool) {

			runtime.LockOSThread()
			defer runtime.UnlockOSThread()

			var err error
			clients[j], err = New("config.toml")
			if err != nil {
				t.Fatalf("failed to find config: %s", err.Error())
			}

			err = clients[j].Connect()
			if err != nil {
				t.Fatalf("failed to connect to cluster: %s", err.Error())
			}

			// Wait until all goroutines finish configuration
			configBarrier.Done()
			configBarrier.Wait()

			for {
				msg, ok := <-requests
				if !ok {
					finishedBarrier.Done()
					break
				}

				clients[j].Broadcast(msg)

				// Avoid Burst requisitions. Could ve been done waiting for servers repply, but avoided
				// that logic for the sake of simplicity
				//time.Sleep(500 * time.Millisecond)
			}
			finishedBarrier.Done()
		}(i, requests, signal)
	}
	finishedBarrier.Wait()

	for _, v := range clients {
		v.Disconnect()
	}
}

func generateStrRequests(reqs chan<- string, signal <-chan bool) {
	runtime.LockOSThread()
	defer runtime.UnlockOSThread()

	msg := "@\n"
	for {
		select {
		case reqs <- msg:
			// ...
		case <-signal:
			close(reqs)
			return
		}
	}
}

func killWorkers(timeSeconds int64, signal chan<- bool) {
	t := time.NewTimer(time.Duration(timeSeconds) * time.Second)
	<-t.C
	signal <- true
}
